import { Component } from "react";

class CountClick extends Component {
    constructor (props) {
        super(props);

        this.state = {
            count: 0
        } 

        // //Cach 1: Su dung bind tro this cua function ve class
        // this.clickChangeHandler = this.clickChangeHandler.bind(this);
    }

    // clickChangeHandler () {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    clickChangeHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render () {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChangeHandler}>Click here</button>
            </div>
        )
    }
}

export default CountClick;